<?php
function tiempohace($valor){
// FORMATOS:
// segundos    desde 1970 (función time())        hace_tiempo('12313214');
// defecto (variable $formato_defecto)        hace_tiempo('12:01:02 04-12-1999');
// tu propio formato                        hace_tiempo('04-12-1999 12:01:02 [n.j.Y H:i:s]');
$formato_defecto="H:i:s j-n-Y";
// j,d = día
// n,m = mes
// Y = año
// G,H = hora
// i = minutos
// s = segundos
	if(stristr($valor,'-') || stristr($valor,':') || stristr($valor,'.') || stristr($valor,',')){
		if(stristr($valor,'[')){
			$explotar_valor=explode('[',$valor);
			$valor=trim($explotar_valor[0]);
			$formato=str_replace(']','',$explotar_valor[1]);
		}else{
			$formato=$formato_defecto;
		}
		$valor = str_replace("-"," ",$valor);
		$valor = str_replace(":"," ",$valor);
		$valor = str_replace("."," ",$valor);
		$valor = str_replace(","," ",$valor);
		$numero = explode(" ",$valor);
		$formato = str_replace("-"," ",$formato);
		$formato = str_replace(":"," ",$formato);
		$formato = str_replace("."," ",$formato);
		$formato = str_replace(","," ",$formato);
		$formato = str_replace("d","j",$formato);
		$formato = str_replace("m","n",$formato);
		$formato = str_replace("G","H",$formato);
		$letra = explode(" ",$formato);
		$relacion[$letra[0]]=$numero[0];
		$relacion[$letra[1]]=$numero[1];
		$relacion[$letra[2]]=$numero[2];
		$relacion[$letra[3]]=$numero[3];
		$relacion[$letra[4]]=$numero[4];
		$relacion[$letra[5]]=$numero[5];
		$valor = mktime($relacion['H'],$relacion['i'],$relacion['s'],$relacion['n'],$relacion['j'],$relacion['Y']);
	}
	$ht = time()-$valor;
	if($ht>=2116800){
	$dia = date('d',$valor);
	$mes = date('n',$valor);
	$año = date('Y',$valor);
	$hora = date('H',$valor);
	$minuto = date('i',$valor);
	$mesarray = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	$fecha = "el $dia de $mesarray[$mes] del $año";
	}
	if($ht<30242054.045){$hc=round($ht/2629743.83);if($hc>1){$s="es";}$fecha="hace $hc mes".$s;}
	if($ht<2116800){$hc=round($ht/604800);if($hc>1){$s="s";}$fecha="hace $hc semana".$s;}
	if($ht<561600){$hc=round($ht/86400);if($hc==1){$fecha="ayer";}if($hc==2){$fecha="antes de ayer";}if($hc>2)$fecha="hace $hc d&iacute;as";}
	if($ht<84600){$hc=round($ht/3600);if($hc>1){$s="s";}$fecha="hace $hc hora".$s;if($ht>4200 && $ht<5400){$fecha="hace m&aacute;s de una hora";}}
	if($ht<3570){$hc=round($ht/60);if($hc>1){$s="s";}$fecha="hace $hc minuto".$s;}
	if($ht<60){$fecha="hace $ht segundos";}
	if($ht<=3){$fecha="ahora";}
return $fecha;
}

function fecha_actual($time){
	$fMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    $fDias = array( 'Domingo', 'Lunes', 'Martes','Miercoles', 'Jueves', 'Viernes', 'Sabado');
    return $fDias[date("w", $time)].", ".date('d', $time)." de ".$fMeses[date('m', $time)-1]." de ".date('Y', $time)." - ".date("H:i:s", $time);
}
function generarCodigo($longitud) {
	$key = '';
	$pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
	$max = strlen($pattern)-1;
	for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
	return $key;
}
function sexo($dato){
	if($dato=="m"){
		$final = "Hombre";
	}
	else {
		$final = "Mujer";
	}
	return $final;
}
function carrera($val1, $val2, $val3, $val4, $val5, $val6, $val7, $val8, $val9, $val10){
	$entrada=array($val1, $val2, $val3, $val4, $val5, $val6, $val7, $val8, $val9, $val10);
	$carrera=array("Medicina, Enfermeria, Trabajo Social, Psicologia", "Publicidad, Marketing, Administración, Negocios", "Educación, Turismo, Literatura, Derecho, Comunicación", "Arte, Arquitetura", "Música, Ing. Sonido", "Ing. Sistemas, Ing. Industrial", "Ing. Minas, Ing. Quimica, Ing. Industrial", "Contabilidad, Economía", "Ing, Mecánica, Ing, Civil", "Ing. Electronica, Escultura");
	$mayor = max($entrada);
	for ($i = 0; $i <= count($entrada)-1; $i++) {
		if($mayor > $entrada[$i]) {
		} else {
			$final = $carrera[$i]."<br>";
		}
	}
	return $final;
}
function titulo($var1, $var2, $var3, $var4){
	if(!empty($var3)){
		$var5 = $var3;
	}
	else {
		$var5 = $var4;
	}
	if(!empty($var1) && !empty($var2)){
		$rptintento = mysql_query("select * from intento where id='".$var1."'");
		$intento = mysql_fetch_array($rptintento);
		if (!empty($intento['id']) and $var2==$intento['codigo'] and $var5==Url_Amigable($intento['nombre']).Url_Amigable($intento['apellido'])) {
			return $intento['nombre']." ".$intento['apellido']." - ";
		}
		else {
			return "no existe! - ";
		}
	}
}
function Url_Amigable($cadena) {
   // Sepadador de palabras que queremos utilizar
   $separador = "";
   // Eliminamos el separador si ya existe en la cadan actual
   $cadena = str_replace($separador, "",$cadena);
   // Convertimos la cadena a minusculas
   $cadena = strtolower($cadena);
   // Remplazo tildes y eñes
   $cadena = strtr($cadena, "áéíóúÁñÑ", "aeiouAnN");
   // Remplazo cuarquier caracter que no este entre A-Za-z0-9 por un espacio vacio
   $cadena = trim(ereg_replace("[^ A-Za-z0-9]", "", $cadena));
   // Inserto el separador antes definido
   $cadena = ereg_replace("[ \t\n\r]+", $separador, $cadena);
   return $cadena;
}
function Calc_Minutos($date1,$date2){
	$diff = abs(strtotime($date2) - strtotime($date1));
	$years   = floor($diff / (365*60*60*24));
	$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
	$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
	$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
	$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
	 return $minuts." - ".$seconds;
}
?>