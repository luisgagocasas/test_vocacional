<?php include "config.php"; include "funciones.php"; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Test Vocacional | Plataforma Educativa - UTP AQP</title>
        <link rel="shortcut icon" href="img/favicon.png"/>
        <meta name="description" content="Tes Vocacional | Plataforma Educativa - UTP AQP">
        <meta property="og:type" content="article"/>
        <meta property="og:url" content="http://test-vocacional.plataforma.edu.pe/"/>
        <meta property="og:image" content="http://plataforma.edu.pe/theme/image.php/aardvark/theme/1379360769/apple-touch-icon"/>
        <meta property="og:site_name" content="<?=titulo($_GET['test'], $_GET['ver'], $_GET['datosp'], $_GET['datospe']); ?>Test Vocacional"/>
        <meta property="fb:admins" content="100003974860941"/>
        <meta name="viewport" content="width=device-width">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans|Dosis" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/estilos.css">
        <link rel="stylesheet" type="text/css" href="css/imprimir.css" media="print" />
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="js/vendor/jquery-1.10.1.min.js"></script>
        <script>
        $(document).ready(function(){
            $("#enviar").click(function(){
                var formulario = $("#frminformacion").serializeArray();
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "guardar.php",
                    data: formulario,
                }).done(function(respuesta){
                    $("#mensaje").html(respuesta.mensaje);
                    limpiarformulario("#frminformacion");
                });
            });
            function limpiarformulario(formulario){
                $(formulario).find('input').each(function() {
                    switch(this.type) {
                        case 'password':
                        case 'text':
                        $(this).val('');
                            break;
                        case 'checkbox':
                        case 'radio':
                            this.checked = false;
                        }
                });
            }
        });
        </script>
    </head>
    <body>
        <div id="cuerpo">
            <div class="cabeza">
                <h1>Test Vocacional</h1>
                <div class="logos">
                    <figure id="logo">
                        <img src="img/jm.png">
                    </figure>
                    <figure id="logo1">
                        <img src="img/plataforma.png">
                    </figure>
                    <figure id="logo2">
                        <img src="img/utp.png">
                    </figure>
                </div>
            </div>
            <section class="proceder">
                <?php
                if(!empty($_POST['transcurso'])){ ?>
                    <?php
                    $pgt1 = $_POST['p101'] + $_POST['p201'] + $_POST['p301'] + $_POST['p401'];
                    $pgt2 = $_POST['p102'] + $_POST['p202'] + $_POST['p302'] + $_POST['p402'];
                    $pgt3 = $_POST['p103'] + $_POST['p203'] + $_POST['p303'] + $_POST['p403'];
                    $pgt4 = $_POST['p104'] + $_POST['p204'] + $_POST['p304'] + $_POST['p404'];
                    $pgt5 = $_POST['p105'] + $_POST['p205'] + $_POST['p305'] + $_POST['p405'];
                    $pgt6 = $_POST['p106'] + $_POST['p206'] + $_POST['p306'] + $_POST['p406'];
                    $pgt7 = $_POST['p107'] + $_POST['p207'] + $_POST['p307'] + $_POST['p407'];
                    $pgt8 = $_POST['p108'] + $_POST['p208'] + $_POST['p308'] + $_POST['p408'];
                    $pgt9 = $_POST['p109'] + $_POST['p209'] + $_POST['p309'] + $_POST['p409'];
                    $pgt10 = $_POST['p110'] + $_POST['p210'] + $_POST['p310'] + $_POST['p410'];
                    ?>
                    <div class="resultado">
                        <div class="liston">
                            <img src="img/resultado_01.png" />
                            <div class="cont">
                                <br><h1>Felicitaciones</h1>
                                <!-- <h4>Recomendamos las siguientes carreras ...</h4> -->
                                <span>
                                <? //carrera($pgt1,$pgt2,$pgt3,$pgt4,$pgt5,$pgt6,$pgt7,$pgt8,$pgt9,$pgt10); ?>
                                <?=$_POST['nombre']; ?> <?=$_POST['apellido']; ?>
                                </span>
                            </div>
                        </div>
                        <img src="img/resultado_02.jpg" class="resultop" />
                        <div class="resulcont">
                            <ul>
                                <li><span>Transcurso del test:</span> <?=tiempohace($_POST['transcurso']); ?></li>
                                <li><span>Inicio:</span> <?=fecha_actual($_POST['transcurso']); ?></li>
                            </ul>
                            <!-- <a class="Esconder_Mostrar">Guardar</a> -->
                            <div class="DivEscondido">
                                </br>
                                <div id="mensaje">
                                    <form id="frminformacion">
                                        <input type="hidden" name="pgt1" value="<?=$pgt1; ?>">
                                        <input type="hidden" name="pgt2" value="<?=$pgt2; ?>">
                                        <input type="hidden" name="pgt3" value="<?=$pgt3; ?>">
                                        <input type="hidden" name="pgt4" value="<?=$pgt4; ?>">
                                        <input type="hidden" name="pgt5" value="<?=$pgt5; ?>">
                                        <input type="hidden" name="pgt6" value="<?=$pgt6; ?>">
                                        <input type="hidden" name="pgt7" value="<?=$pgt7; ?>">
                                        <input type="hidden" name="pgt8" value="<?=$pgt8; ?>">
                                        <input type="hidden" name="pgt9" value="<?=$pgt9; ?>">
                                        <input type="hidden" name="pgt10" value="<?=$pgt10; ?>">
                                        <input type="hidden" name="codigo" value="<?=generarCodigo(6); ?>">
                                        <input type="hidden" name="transcurso" value="<?=$_POST['transcurso']; ?>">
                                        <input type="hidden" name="colegio" value="<?=$_POST['colegio']; ?>">
                                        <label> Nombre: </label>
                                        <input type="text" name="nombre" class="requerido" value="<?=ucwords($_POST['nombre']); ?>" /> <br/>
                                        <label> Apellidos: </label>
                                        <input type="text" name="apellidos" class="requerido" value="<?=ucwords($_POST['apellido']); ?>"  /> <br/>
                                        <label> Correo: </label>
                                        <input type="text" name="email" class="requerido" /><br/>
                                        <label> Sexo: </label>
                                        <label><input type="radio" name="sexo" value="f" /> Femenino</label>
                                        <label><input type="radio" name="sexo" value="m" checked /> Masculino</label>
                                        <br/>
                                        <input type="button" id="enviar" value="Guardar" name="enviar" />
                                    </form>
                                </div>
                            </div>
                        </div>
                        <img src="img/resultado_03.jpg" class="resultbutton" />
                    </div>
                    <br>
                <?php
                }
                else { ?>
                    noce puede mostrar
                <?php
                }
                ?>
            </section>
            <div class="pies">
                <a href="http://plataforma.edu.pe/" target="_blank">Plataforma.edu.pe</a><br>
                Todos los derechos reservados.
            </div>
        </div>
        <br>
        <script src="js/main.js"></script>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-41465403-5', 'plataforma.edu.pe');
        ga('send', 'pageview');
        </script>
    </body>
</html>