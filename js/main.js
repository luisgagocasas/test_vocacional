function logoefec2(){
    $('#logo2').removeClass().addClass("animated bounceInDown");
    window.setTimeout( function(){
        $('#logo2').removeClass()},
        1300
    );
}
function logoefec1(){
    $('#logo1').removeClass().addClass("animated bounceInDown");
    window.setTimeout( function(){
        $('#logo1').removeClass()},
        1300
    );
}
function logoefec(){
    $('#logo').removeClass().addClass("animated bounceInDown");
    window.setTimeout( function(){
        $('#logo').removeClass()},
        1300
    );
}
$(document).ready(function(){
    // Efecto en los logos css
    $('#logo, #logo1, #logo2').addClass("animated bounceInDown");
    window.setTimeout( function(){
        $('#logo, #logo1, #logo2').removeClass()},
        1300
    );
    $('#logo').click(function(){
        logoefec();
    });
    $('#logo1').click(function(){
        logoefec1();
    });
    $('#logo2').click(function(){
        logoefec2();
    });
});