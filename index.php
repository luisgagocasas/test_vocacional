<?php include "config.php"; include "funciones.php"; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?=titulo($_GET['test'], $_GET['ver'], $_GET['datosp'], $_GET['datospe']); ?>Test Vocacional | Plataforma Educativa - UTP AQP</title>
        <link rel="shortcut icon" href="img/favicon.png"/>
        <meta name="description" content="Tes Vocacional | Plataforma Educativa - UTP AQP">
        <meta property="og:type" content="article"/>
        <meta property="og:url" content="http://test-vocacional.plataforma.edu.pe/"/>
        <meta property="og:image" content="http://plataforma.edu.pe/theme/image.php/aardvark/theme/1379360769/apple-touch-icon"/>
        <meta property="og:site_name" content="<?=titulo($_GET['test'], $_GET['ver'], $_GET['datosp'], $_GET['datospe']); ?>Test Vocacional"/>
        <meta property="fb:admins" content="100003974860941"/>
        <meta name="viewport" content="width=device-width">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans|Dosis" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/estilos.css">
        <link rel="stylesheet" type="text/css" href="css/imprimir.css" media="print" />
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <div id="cuerpo">
            <div class="cabeza">
                <h1>Test Vocacional</h1>
                <div class="logos">
                    <figure id="logo">
                        <img src="img/jm.png">
                    </figure>
                    <figure id="logo1">
                        <img src="img/plataforma.png">
                    </figure>
                    <figure id="logo2">
                        <img src="img/utp.png">
                    </figure>
                </div>
            </div>
            <?php if (!isset($_GET['test'])) { ?>
            <!-- <section class="publicidad">
                <div class="publi1">
                    Exámen de admisión 17 Noviembre.
                </div>
                <div class="publi2">
                    Exámen preferencial para alumnos de 5 de secundaria 30 Noviembre.
                </div>
            </section> -->
            <div class="superior">
                <div class="imagen">
                    <img src="img/img001.jpg" />
                </div>
                <div class="mensajebd">
                    <img src="img/nota.png" />
                    <p>
                        A continuación se te presenta una serie de situaciones. Después de leerla, establece un orden de prioridad para cada situación, escribiendo el número <b>10</b> para la que te resulte más fácil de hacer, el <b>9</b> para la que le sigue en grado de dificultad y asi sucesivamente.
                    </p>
                </div>
            </div>
            <form action="procesar.php" method="post">
                <input type="hidden" name="transcurso" value="<?=time(); ?>">
                <article class="pregunta">
                    <div class="datospersonales">
                        <h2>Datos Personales:</h2>
                        <input type="text" name="nombre" required title="Mi Nombre" placeholder="Nombre">
                        <input type="text" name="apellido" required title="Mis Apellidos" placeholder="Apellidos">
                        <input type="text" name="colegio" required title="Mi Colegio" placeholder="Colegio">
                    </div>
                </article>
                <article class="pregunta">
                    <h2>En un día libre ¿Cómo ordenarías las siguientes actividades según <u>tu capacidad</u> para realizarlas con destreza?</h2>
                    <div class="preguntita">
                        1) .- Cuidar a un amigo o pariente enfermo
                        <label>
                            <select name="p101">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        2) .- Organizar una fiesta
                        <label>
                            <select name="p102">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        3) .- Escribir un mensaje para un ser querido
                        <label>
                            <select name="p103">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        4) .- Pintar un cuadro
                        <label>
                            <select name="p104">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        5) .- Participar con tu grupo musical preferido en un concierto
                        <label>
                            <select name="p105">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        6) .- Poner en orden tu colección favorita
                        <label>
                            <select name="p106">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        7) .- Realizar un experimento científico
                        <label>
                            <select name="p107">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        8) .- Resolver problemas de matemáticas
                        <label>
                            <select name="p108">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        9) .- Reparar algún aparato de tu preferencia
                        <label>
                            <select name="p109">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        10) .- Realizar un trabajo muy fino con tus manos
                        <label>
                            <select name="p110">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                </article>
                <!-- pregunta 2 -->
                <article class="pregunta">
                    <h2>Si te ofrecieran las siguientes opciones de trabajo (Ganando lo mismo en cualquiera de ellas), ¿Cómo ordenarías las siguientes opciones según <u>la destreza</u> que tú sabes que tienes para desempeñar cada uno de ellos?</h2>
                    <div class="preguntita">
                        1) .- Enseñar a estudiantes desaventajados
                        <label>
                            <select name="p201">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        2) .- Organizar a un grupo de jóvenes
                        <label>
                            <select name="p202">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        3) .- Escribir las memorias de un actor famoso
                        <label>
                            <select name="p203">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        4) .- Pintar un mural
                        <label>
                            <select name="p204">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        5) .- Tocar tu instrumento musical preferido
                        <label>
                            <select name="p205">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        6) .- Clasificar medicinas
                        <label>
                            <select name="p206">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        7) .- Hacer un experimento con plantas
                        <label>
                            <select name="p207">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        8) .- Calcular el presupuesto de una familia
                        <label>
                            <select name="p208">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        9) .- Contruir maquetas
                        <label>
                            <select name="p209">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        10) .- Reparar relojes
                        <label>
                            <select name="p210">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                </article>
                <!-- Pregunta 3 -->
                <article class="pregunta">
                    <h2>Si tuvieras que hacer una labor social, ¿en cuál de estos ambientes preferirias trabajar? Ordénalos tomando en cuenta <u>tu capacidad o destreza</u> para desempeñarte en cada opción.</h2>
                    <div class="preguntita">
                        1) .- Hospital
                        <label>
                            <select name="p301">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        2) .- Agencia de publicidad
                        <label>
                            <select name="p302">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        3) .- Editorial
                        <label>
                            <select name="p303">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        4) .- Estudio de un pintor
                        <label>
                            <select name="p304">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        5) .- Orquesta de tu ciudad
                        <label>
                            <select name="p305">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        6) .- Biblioteca
                        <label>
                            <select name="p306">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        7) .- Laboratorio
                        <label>
                            <select name="p307">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        8) .- Despacho de contadores
                        <label>
                            <select name="p308">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        9) .- Taller mecánico
                        <label>
                            <select name="p309">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        10) .- Consultorio de un dentista
                        <label>
                            <select name="p310">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                </article>
                <!-- pregunta 4 -->
                <article class="pregunta">
                    <h2>Imagínate cómo serás y lo que serás dentro de diez años, ¿cómo ordenarías las siguientes opciones de acuerdo con tus habilidades?</h2>
                    <div class="preguntita">
                        1) .- Director de ayuda, en caso de desastre, de la Cruz Roja Internacional
                        <label>
                            <select name="p401">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        2) .- Gerencia de mercadotecnia de una compañía.
                        <label>
                            <select name="p402">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        3) .- Articulista de un periódico
                        <label>
                            <select name="p403">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        4) .- Diseñador de las portadas de una revista
                        <label>
                            <select name="p404">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        5) .- Intérprete musical del género de tu preferencia
                        <label>
                            <select name="p405">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        6) .- Diseñador de software
                        <label>
                            <select name="p406">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        7) .- Coordinador de un grupo de vanguardia en la ciencia
                        <label>
                            <select name="p407">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        8) .- Contador general de una empresa
                        <label>
                            <select name="p408">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        9) .- Autoridad en la contrucción de ciertas estructuras arquitectónicas
                        <label>
                            <select name="p409">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                    <div class="preguntita">
                        10) .- Experto constructor de maquetas o modelos a escala
                        <label>
                            <select name="p410">
                                <optgroup label="Seleccione">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </optgroup>
                            </select>
                        </label>
                    </div>
                </article>
                <center><input name="enviar" class="enviar" type="submit" value="Procesar ...." /></center>
            </form>
            <?php
            }
            if($_GET['test']=="resultados") { ?>
                <section class="loginresultados">
                    <h2>Todos los Resultados del Test</h2>
                    <?php
                    if(empty($_POST['usuario'])){ ?>
                        <form method="post">
                            <label> Usuario: </label>
                            <input type="text" name="usuario" /> <br/>
                            <label> Contraseña: </label>
                            <input type="password" name="pass" />
                            <br/>
                            <input type="submit" value="Entrar" />
                        </form>
                    <?php
                    }
                    else if($_POST['usuario']=="administrador" and $_POST['pass']=="25.de.diciembre" or $_POST['usuario']=="luisgago" and $_POST['pass']=="1989sunway"){ ?>
                        <h3>Bienvenido: <em>Administrador</em></h3>
                        <?php
                        $rpttest = mysql_query("select * from intento");
                        $registros = mysql_num_rows($rpttest);
                        echo "Total: ".$registros;  ?><br>
                        <ul class="titulos">
                            <li>Nombre</li>
                            <li>Apellidos</li>
                            <li>Sexo</li>
                            <li>Correo</li>
                            <li>F. de Inicio</li>
                            <li>Duración</li>
                            <li>Carrera</li>
                        </ul>
                        <?php
                        while($intentos = mysql_fetch_array($rpttest)){
                            echo "<ul class=\"resultados\">";
                            echo "<li><a href=\"http://test-vocacional.plataforma.edu.pe/".$intentos['id']."-".$intentos['codigo']."-".Url_Amigable($intentos['nombre']).Url_Amigable($intentos['apellido'])."/\" target=\"_blank\">".$intentos['nombre']."</a></li>\n";
                            echo "<li>".$intentos['apellido']."</li>\n";
                            echo "<li>".sexo($intentos['sexo'])."</li>\n";
                            echo "<li>".$intentos['email']."</li>\n";
                            echo "<li>".fecha_actual($intentos['transcurso'])."</li>\n";
                            echo "<li>".fecha_actual($intentos['fecha'])."</li>\n";
                            //echo "<li>".Calc_Minutos("2008-11-01 22:45:00", "2009-12-04 13:44:01")."</li>";
                            echo "<li>".carrera($intentos['sumpre1'], $intentos['sumpre2'], $intentos['sumpre3'], $intentos['sumpre4'], $intentos['sumpre5'], $intentos['sumpre6'], $intentos['sumpre7'], $intentos['sumpre8'], $intentos['sumpre9'], $intentos['sumpre10'])."</li>\n";
                            echo "</ul>";
                        }
                        ?>
                        <br><br><br>
                        <center><a href="?test=resultados" class="salir">Salir</a></center>
                    <?php
                    }
                    else{
                        echo "Es incorrecto!";
                    }
                    ?>
                </section>
            <?php }
            if($_GET['test'] && $_GET['ver'] && $_GET['datosp']) {
                $rptintento = mysql_query("select * from intento where id='".$_GET['test']."'");
                $intento = mysql_fetch_array($rptintento);
                if (!empty($intento['id']) and $_GET['ver']==$intento['codigo'] and $_GET['datosp']==Url_Amigable($intento['nombre']).Url_Amigable($intento['apellido'])) { ?>
                    <section class="proceder">
                        <div class="resultado">
                            <div class="liston">
                                <img src="img/resultado_01.png" />
                                <div class="cont">
                                    <h1>Vocación de <?=$intento['nombre']; ?> <?=$intento['apellido']; ?></h1>
                                    <h4>Recomendamos las siguientes carreras ...</h4>
                                    <span>
                                        <?=carrera($intento['sumpre1'],$intento['sumpre2'],$intento['sumpre3'],$intento['sumpre4'],$intento['sumpre5'],$intento['sumpre6'],$intento['sumpre7'],$intento['sumpre8'],$intento['sumpre9'],$intento['sumpre10']); ?>
                                    </span>
                                </div>
                            </div>
                            <img src="img/resultado_02.jpg" class="resultop" />
                            <div class="resulcont">
                                <img src="http://www.gravatar.com/avatar.php?gravatar_id=<?=md5($intento['email']); ?>&default=<?=urlencode("http://img23.imageshack.us/img23/2706/m7uf.png"); ?>&size=150" class="perfil" title="Foto de gravatar.com" />
                                <ul class="datos">
                                    <li><span>Sexo:</span> <?=sexo($intento['sexo']); ?></li>
                                    <li><span>Colegio:</span> <b><?=$intento['colegio']; ?></b></li>
                                    <li><span>Creado:</span> <?=tiempohace($intento['fecha']); ?></li>
                                    <li><span>Fecha que empezó:</span> <?=fecha_actual($intento['transcurso']); ?></li>
                                    <li><span>Fecha que culminó:</span> <?=fecha_actual($intento['fecha']); ?></li>
                                </ul>
                                <div class="compartir">
                                    <a href="http://www.facebook.com/sharer.php?u=http://test-vocacional.plataforma.edu.pe/<?=$intento['id']."-".$intento['codigo']."-".Url_Amigable($intento['nombre']).Url_Amigable($intento['apellido']); ?>/&t=Tes Vocacional de <?=$intento['nombre']; ?> <?=$intento['apellido']; ?> | Plataforma Educativa UTP AQP" target="_blank">
                                        <img src="img/fb.png">
                                        Compartir
                                    </a>
                                    <a href="http://twitter.com/?status=http://test-vocacional.plataforma.edu.pe/<?=$intento['id']."-".$intento['codigo']."-".Url_Amigable($intento['nombre']).Url_Amigable($intento['apellido']); ?>/" target="_blank">
                                        <img src="img/tw.png">
                                        Compartir
                                    </a>
                                    <a href="https://plus.google.com/share?url=http://test-vocacional.plataforma.edu.pe/<?=$intento['id']."-".$intento['codigo']."-".Url_Amigable($intento['nombre']).Url_Amigable($intento['apellido']); ?>/" target="_blank">
                                        <img src="img/g+.png">
                                        Compartir
                                    </a>
                                    <a href="http://test-vocacional.plataforma.edu.pe/<?=$intento['id']."-".$intento['codigo']."-".Url_Amigable($intento['nombre']).Url_Amigable($intento['apellido']); ?>/imprimir/">
                                        <img src="img/print.png">
                                        Imprimir
                                    </a>
                                </div>
                            </div>
                            <img src="img/resultado_03.jpg" class="resultbutton" />
                        </div>
                        <br>
                    </section>
                    <?php
                }
                else { echo "<em>no existe!</em>"; }
            }
            if($_GET['test'] && $_GET['ver'] && $_GET['datospe'] && $_GET['imprimir']=="true") {
                $rptintento = mysql_query("select * from intento where id='".$_GET['test']."'");
                $intento = mysql_fetch_array($rptintento);
                if (!empty($intento['id']) and $_GET['ver']==$intento['codigo'] and $_GET['datospe']==Url_Amigable($intento['nombre']).Url_Amigable($intento['apellido'])) { ?>
                    <script> window.print(); </script>
                    <section class="proceder">
                        <center><h2>Informe de Orientación Vocacional</h2></center>
                        <p class="justificado">El presente documento informa sobre el resultado del proceso de orientación vocacional.<br>El Sr/Srta Procedente del colegio: <span class="mgrande3"><?=$intento['colegio']; ?></span><br><center><span class="mgrande"><?=$intento['nombre']; ?> <?=$intento['apellido']; ?></span></center></p>
                        <p class="justificado">En la evaluacion realizada se tomaron los siguientes criterios:<p>
                        <ul class="listape">
                            <li>A) Aptitudes intelectuales, que define la capacidad cognitiva de la persona para resolver problemas y tomar decisiones dentro de un dominio (numérico, verbal o espacial) determinado.</li>
                            <li>B) Personalidad, que define la satisfacción personal en el sentido actitudinal que se encontrará en distintos tipos de actividad profesionales según sus inclinaciones y estructuras socio-emocionales.</li>
                            <li>C) Intéreses, que define la inclinación cognitiva y emocional sobre actividades concretas, lo que permitirá un elevado nivel de motivación por el trabajo y el despliegue de la creatividad.</li>
                        </ul>
                        <p class="justificado">Según los resultados de las evaluaciones personales aplicadas se determina que la carrera profesional que más se adecúa a su perfil psicológico intelectual, a su tipo de personalidad y a sus intereses profesionales es la carrera de <span class="mgrande2"><?=carrera($intento['sumpre1'],$intento['sumpre2'],$intento['sumpre3'],$intento['sumpre4'],$intento['sumpre5'],$intento['sumpre6'],$intento['sumpre7'],$intento['sumpre8'],$intento['sumpre9'],$intento['sumpre10']); ?></span>, en la que podría estudiar, en la Universidad Tecnologica del Perú - Filial Arequipa.</p>
                        <?php
                        $fMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                        ?>
                        <p>Arequipa, <?=date('d')." de ".$fMeses[date('m')-1]." de ".date('Y'); ?></p>
                        <div class="departamento">
                            <p>
                                Departamento Psicopedagógico<br>
                                Universidad Tecnológica del Perú<br>
                                Filial Arequipa
                            </p>
                        </div>
                        <br>
                        <center><p class="justificado masinformacion">Para más información visitenos en la Merced 209 Cercado, en el departamento psicopedagógico.</p></center>
                    </section>
                    <?php
                }
                else { echo "<em>no existe!</em>"; }
            }
            ?>
            <div class="pies">
                <a href="http://plataforma.edu.pe/" target="_blank">Plataforma.edu.pe</a><br>
                Todos los derechos reservados.
            </div>
        </div>
        <br>
        <script src="js/vendor/jquery-1.10.1.min.js"></script>
        <script src="js/main.js"></script>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-41465403-5', 'plataforma.edu.pe');
        ga('send', 'pageview');
        </script>
    </body>
</html>